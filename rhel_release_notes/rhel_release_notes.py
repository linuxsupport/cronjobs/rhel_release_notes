#!/usr/bin/python3
""" Script to update the RHEL release notes on linux.cern.ch, automagically """

import os
import tempfile
import sys
import subprocess
import re
import shutil
import smtplib
from datetime import datetime
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import requests

ADMIN_EMAIL = os.getenv("ADMIN_EMAIL")
GIT_PROJECT_PATH = os.getenv("GIT_PROJECT_PATH")
RELEASES = os.getenv("RELEASES")
SMTPRELAY = os.getenv("SMTPRELAY")


def do_execute(cmd, tdir="/tmp", return_output=False, return_error=False):
    """execute stuff"""
    # pylint: disable=consider-using-with
    process = subprocess.Popen(
        cmd, cwd=tdir, stderr=subprocess.PIPE, shell=True, stdout=subprocess.PIPE
    )
    out, err = process.communicate()
    if process.wait() != 0:
        print(f"Failed to execute: {cmd}")
        sys.exit(process.wait())
    if return_output:
        return out.decode().strip()
    if return_error:
        return err.decode().strip()
    return True


def send_email(e_subject, e_body, e_to, e_from="linux.support@cern.ch"):
    """send an email"""
    server = smtplib.SMTP(SMTPRELAY)
    msg = MIMEMultipart()
    msg["Subject"] = e_subject
    msg["From"] = e_from
    msg["To"] = e_to
    msg.add_header("reply-to", "noreply.Linux.Support@cern.ch")
    e_body = MIMEText(f"{e_body}", _subtype="plain")
    msg.attach(e_body)
    server.sendmail(e_from, e_to, msg.as_string())


def main():
    """main class, yo"""
    for r in RELEASES.split(","):
        # Find out what is the latest release
        req = requests.get(f"https://linuxsoft.cern.ch/cern/rhel/.{r}-latest-release")
        if req.status_code == 404:
            print(
                f"No CERN release for {r} exists, continuing"
            )
            continue
        latest_release = req.text.strip()
        release_note = f"https://gitlab.cern.ch/{GIT_PROJECT_PATH}/-/raw/master/docs/rhel/rhel{r}/Red_Hat_Enterprise_Linux-{r}-{latest_release}_Release_Notes-en-US.pdf"
        req = requests.get(release_note)
        if req.status_code == 200:
            print(
                f"We already have release notes defined for RHEL{latest_release}, continuing"
            )
            continue
        # Check if we do not already have a MR open
        check_branch_exists = f"https://gitlab.cern.ch/{GIT_PROJECT_PATH}/-/raw/releasenotes_{latest_release}/README.md"
        req = requests.get(check_branch_exists)
        if req.status_code == 200:
            print(
                f"A MR already exists for {GIT_PROJECT_PATH}/{latest_release}, but the admins must be slow in merging it. Quietly leaving the party"
            )
            continue
        release_url = f"https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{r}/pdf/{latest_release}_release_notes/red_hat_enterprise_linux-{r}-{latest_release}_release_notes-en-us.pdf"
        pdf = requests.get(release_url, stream=True)
        if pdf.status_code != 200:
            print(
                f"Upstream release notes for {latest_release} don't appear to exist, yet"
            )
            continue
        # check we have a file
        tmpdir = tempfile.mkdtemp()
        output_file = f"{tmpdir}/Red_Hat_Enterprise_Linux-{r}-{latest_release}_Release_Notes-en-US.pdf"
        with open(output_file, "wb") as of:
            for chunk in pdf.iter_content(chunk_size=128):
                of.write(chunk)
        if "PDF" not in do_execute(
            f"/usr/bin/file -b {output_file}", return_output=True
        ):
            print("Downloaded file is not a PDF, bailing")
            continue
        print("Cloning linux.cern.ch")
        git_linux_url = f"https://:@gitlab.cern.ch:8443/{GIT_PROJECT_PATH}.git"
        do_execute(f"git clone {git_linux_url}", f"{tmpdir}")
        do_execute(
            f"git checkout -b releasenotes_{latest_release}", f"{tmpdir}/linux.cern.ch"
        )
        print("Making ammendments")
        # If we have a new major release (eg: "10"), this directory may not already exist
        if not os.path.exists(f"{tmpdir}/linux.cern.ch/docs/rhel/rhel{r}"):
            os.makedirs(f"{tmpdir}/linux.cern.ch/docs/rhel/rhel{r}")
        shutil.copy(
            f"{tmpdir}/Red_Hat_Enterprise_Linux-{r}-{latest_release}_Release_Notes-en-US.pdf",
            f"{tmpdir}/linux.cern.ch/docs/rhel/rhel{r}/Red_Hat_Enterprise_Linux-{r}-{latest_release}_Release_Notes-en-US.pdf",
        )
        do_execute(
            f"git add docs/rhel/rhel{r}/Red_Hat_Enterprise_Linux-{r}-{latest_release}_Release_Notes-en-US.pdf",
            f"{tmpdir}/linux.cern.ch",
        )
        with open(
            f"{tmpdir}/linux.cern.ch/docs/rhel/rhel{r}/{datetime.today().strftime('%Y%m%d')}.md",
            "w",
            encoding="utf-8",
        ) as f:
            f.writelines(
                [
                    f"#### RHEL {latest_release}\n",
                    "\n",
                    f"* Installation target: **RHEL_{r}_{str(latest_release).split('.')[1]}_X86_64**\n",
                    f"* Installation path:   **http://linuxsoft.cern.ch/cern/rhel/{latest_release}/baseos/x86_64/os**\n",
                    f"* Release notes:       **[RELEASE-NOTES-{latest_release}-x86_64](Red_Hat_Enterprise_Linux-{r}-{latest_release}_Release_Notes-en-US.pdf)**",
                ]
            )
        do_execute(
            f"git add docs/rhel/rhel{r}/{datetime.today().strftime('%Y%m%d')}.md",
            f"{tmpdir}/linux.cern.ch",
        )
        print("Making a commit")
        do_execute(
            f"git -c user.name='CERN Linux Droid' -c user.email='linux.ci@cern.ch' commit -am 'Add release notes for RHEL{latest_release}'",
            f"{tmpdir}/linux.cern.ch",
        )
        print("Pushing branch and making a MR")
        # But ... Let's make sure we can actually push via kerberos
        do_execute("git config --global http.emptyAuth true")
        push_output = do_execute(
            f"git push origin releasenotes_{latest_release} -o merge_request.create -o merge_request.merge_when_pipeline_succeeds",
            f"{tmpdir}/linux.cern.ch",
            return_error=True,
        )
        merge_request_url = None
        for l in push_output.splitlines():
            if "merge_request" in l:
                m = re.search(r"https:\/\/(\S*)", l)
                merge_request_url = m.group(0)
        if merge_request_url is not None:
            print("Sending an email")
            body = f"Dear admins,\n\nToday Red Hat have released the release notes for RHEL{latest_release}.\n\nNo need to stress! I as usual have done all the work for you. For your information, this MR {merge_request_url} was merged.\n\nHave a good day, I guess.\n\nBest regards,\nCERN Linux Droid\n(on behalf of the friendly humans of Linux Support)"
            subject = f"MR: Add release notes for RHEL{latest_release}"
            send_email(subject, body, ADMIN_EMAIL)


if __name__ == "__main__":
    main()
