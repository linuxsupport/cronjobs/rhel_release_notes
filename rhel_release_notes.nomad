job "${PREFIX}_rhel_release_notes" {
  datacenters = ["*"]
  
  type = "batch"

  periodic {
    cron = "${SCHEDULE}"
    time_zone = "Europe/Zurich"
    prohibit_overlap = true
  }

  task "${PREFIX}_rhel_release_notes" {
    driver = "docker"

    config {
      image = "https://gitlab-registry.cern.ch/linuxsupport/cronjobs/rhel_release_notes/rhel_release_notes:${CI_COMMIT_SHORT_SHA}"

      logging {
        config {
          tag = "${PREFIX}_rhel_release_notes"
        }
      }
    }
    env {
      LINUXCI_USER = "$LINUXCI_USER"
      LINUXCI_PWD = "$LINUXCI_PWD"
      NOMAD_ADDR = "$NOMAD_ADDR"
      ADMIN_EMAIL = "$ADMIN_EMAIL"
      GIT_PROJECT_PATH = "$GIT_PROJECT_PATH"
      RELEASES = "$RELEASES"
      SMTPRELAY = "$SMTPRELAY"
      TAG = "${PREFIX}_rhel_release_notes"
    }

    resources {
      cpu = 1000 # Mhz
      memory = 1024 # MB
    }

  }
}
